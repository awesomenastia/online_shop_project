from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    date_of_birth = models.DateField(verbose_name="Дата рождения", blank=True, null=True)
    mobile_phone = models.CharField(verbose_name="Мобильный телефон", max_length=15, default=None)
    GENDER = (
        ('N', "Не выбрано"),
        ('M', 'Мужчина'),
        ('F', 'Женщина'),
    )
    gender = models.CharField(verbose_name="Пол",max_length=7, choices=GENDER, default=None)

    class Meta:
        verbose_name = "Профиль"
        verbose_name_plural = "Профили"

    def __str__(self):
        return 'Профиль для пользователя {}'.format(self.user.username)
